'use strict'
const express = require('express');
const serv = require('express')();
const serverHttp = require('http').Server(serv);
module.exports.io = require('socket.io')(serverHttp);
const path = require('path');

// 2) Inicia el socket
require(path.join(__dirname, './components/chat/socket'));

// 3) Inicia electron
require(path.join(__dirname, './components/chat/electron-chat'));

// 4) Inicia un servidor de node y escucha las conexiones
serverHttp.listen(3000, () => {
    console.log('server running on port', 3000)
});

//5) Inicia una vista de servidor
serv.use(express.static(path.join(__dirname, './views/client')));

