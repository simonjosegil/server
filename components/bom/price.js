const path = require('path');
const { Response$, GetBrowser$ } = require(path.join(__dirname, '../chat/eventemitter'));

let browser;


// 14) Pasa el navegador automatizado de Puppeteer al script automatizador
GetBrowser$.once('msg', (msg) => {
    browser = msg;
}) 

// 15)  Recibe el configId de opt y se le indica la url de Destino
function getPrice(info) {
    let user = info.user.trim();
    let pNumber = info.text.trim();
    matrix = info.content;
    const message2 = new Array();
    let app = info.app.trim();
    let positionJ = info.positionJ
    let url2 = 'https://myprs.glb.itcs.hpicorp.net/myprs/internal/myprsReport.do';
    vBom2(pNumber, user, matrix, message2, app, positionJ, url2);
}


async function vBom2(pNumber, user, matrix, message2, app, positionJ, url2){
    page3 = await browser.newPage();
 try{
    await page3.goto((url2), {waitUntil: 'load', timeout: 0});
    await page3.waitForSelector('#itemsView', {waitUntil: 'load', timeout: 400000});
    await page3.waitForTimeout(12000);
    await page3.click('a[href="javascript:ClearBasket();"]');   
    await page3.click('#itemsView');    
    for (x of matrix) {
        await page3.type('#itemsView', x+"\n", { delay: 15 });
    };
    await page3.click('input[value="Submit"]');   
    await page3.waitForSelector('#exportTyp', {waitUntil: 'load', timeout: 400000});
    console.log("Ok clear");
    tableProcess(page3, pNumber, user, positionJ); 
    /*  if(await page3.$('option[value="ProductID"]') !== null){
        await page3.click('input[value="Submit"]');   
    }else{} */
}
catch (e) {
    page3.close();
    const msg2 = [{"type": 3}, {"name": 'error'}, {"ALERT": 'Error while loading SKU(PRICE)', "ALERTA": 'Error al cargar SKU(SAP)'}];
    Response$.emit('message', msg2); 
    throw new Error(`Servicio no disponible`, 503) 
}

}

async function tableProcess(page3, pNumber, user, positionJ){
    const selector = '#exportTyp > tbody > tr';
    const resultData = await page3.$$eval(selector, rows => {
        return Array.from(rows, row => {
            const columns = row.querySelectorAll('td');
            return Array.from(columns, column => column.innerText.trim());
        });
    });
    createMatrix(resultData, pNumber, positionJ);
   page3.close();
}


function createMatrix(resultData, pNumber, positionJ){
    const matrix = new Array();
    const message = new Array();
    const ele = [];

    for(let i=0; i < resultData.length; ++i){
            //ele.push(resultData[i][2])
            matrix.push({"av": resultData[i][0],"description": resultData[i][1]})
    }
    //console.log(ele);
    console.log(matrix);
    //console.log(message);
    message.push({"type": 7}, {"name": pNumber}, {"ALERT": matrix}, {"positionJ": positionJ} );
    Response$.emit('message', message);
}

module.exports = { getPrice };
