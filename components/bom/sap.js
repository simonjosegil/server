const path = require('path');
const { Response$, GetBrowser$ } = require(path.join(__dirname, '../chat/eventemitter'));
puppeteer = require('puppeteer');


let browser;

// 14) Pasa el navegador automatizado de Puppeteer al script automatizador
GetBrowser$.once('msg', (msg) => {
    browser = msg != null ? msg : browser;
    //browser = msg;
}) 


function getSkuSap(info) {
    let user = info.user;
    const sku = info.text.trim();
    
    console.log(sku);
    let url = 'https://hpibiplb.corp.hpicloud.net/BOE/OpenDocument/opendoc/openDocument.jsp?sIDType=CUID&iDocID=Flih8F13_QgAX2sAAADX4KwRAFBWmGHL';
    skuProcess(sku, url, user);
}



// 16) Script automatizador va hasta la tabla utilizando la URL previamente procesada junto con el sku
async function skuProcess(sku, url, user){
let page2 = await browser.newPage();
    try{
        await page2.goto((url), {waitUntil: 'load', timeout: 0});
        await page2.waitForSelector('.yui-skin-sam', {waitUntil: 'load', timeout: 400000});
        await page2.click('.yui-skin-sam');     
        await page2.waitForSelector('#openDocChildFrame');

        const elementHandle = await page2.$('#openDocChildFrame');
        const frame = await elementHandle.contentFrame();

        await frame.waitForSelector('#__item79-__xmlview15--promptsList-1', {waitUntil: 'load', timeout: 400000});
        const aHandle = await frame.evaluateHandle(() => document.body);
        const resultHandle = await frame.evaluateHandle(body => body.innerHTML, aHandle);
        await resultHandle.dispose();
        
        await page2.waitForTimeout(5000);
        await frame.focus('#__item79-__xmlview15--promptsList-0-content');
        await frame.click('#__item79-__xmlview15--promptsList-0-content');
        await page2.waitForTimeout(1000);
        await frame.click('#__xmlview15--lovPanelView--search-I');
        await frame.type('#__xmlview15--lovPanelView--search-I', sku, { delay: 20 });
        await frame.click('#__button87-inner');
        await frame.click('#__button92-inner');
        await page2.waitForTimeout(1000);
        await frame.click('#__button78-inner');
        await frame.waitForSelector('#container_openDocPage_1', {waitUntil: 'load', timeout: 400000});
        await frame.waitForSelector('#viewopenDocPage_1wiseWorkbenchView', {waitUntil: 'load', timeout: 400000});
        await frame.waitForSelector('#__page3', {waitUntil: 'load', timeout: 400000});
        await frame.waitForSelector('#__page3-cont', {waitUntil: 'load', timeout: 400000});
        await frame.waitForSelector('#viewopenDocPage_1wiseWorkbenchView--doubleSidePanelContainer', {waitUntil: 'load', timeout: 400000});
        await frame.waitForSelector('#viewopenDocPage_1wiseWorkbenchView--doubleSidePanelContainer-content-0', {waitUntil: 'load', timeout: 400000});
        await frame.waitForSelector('#viewopenDocPage_1wiseWorkbenchView--doubleSidePanelContainer-content-0', {waitUntil: 'load', timeout: 400000});
        await frame.waitForSelector('#__xmlview6', {waitUntil: 'load', timeout: 400000});
        await frame.waitForSelector('#__item24', {waitUntil: 'load', timeout: 400000});
        await page2.waitForTimeout(15000);
        await frame.focus('#__item24');
        await frame.click('#__item24');
        await frame.waitForSelector('#__item24', {waitUntil: 'load', timeout: 400000});
        await frame.waitForSelector('#__item24', {waitUntil: 'load', timeout: 400000});
        await frame.waitForSelector('table[idref="LS.1Lw"] > tbody > tr');
        console.log('Tabla ok');
        tableProcess(page2, sku, user, frame); 
    }
    catch (e) {
        page2.close();
        const msg2 = [{"type": 3}, {"name": 'error'}, {"ALERT": 'Error while loading SKU(SAP)', "ALERTA": 'Error al cargar SKU(SAP)'}];
        Response$.emit('message', msg2); 
        throw new Error(`Servicio no disponible`, 503) 
    }
} 



async function tableProcess(page2, sku, user, frame){
    const selector = 'table[idref="LS.1Lw"] > tbody > tr';
    const resultData = await frame.$$eval(selector, rows => {
        return Array.from(rows, row => {
            const columns = row.querySelectorAll('td');
            return Array.from(columns, column => column.innerText.trim());
        });
    });
    createMatrix(resultData, sku);
   page2.close();
}




// 17) Procesa todas las columnas de una tabla




// 18) Las columnas se seleccionan a utilizarse de la tabla, se insertan en objeto Json y se responden al websocket
function createMatrix(resultData, sku){
    const matrix = new Array();
    const message = new Array();
    const ele = [];

    for(let i=1; i < resultData.length; ++i){

        if((resultData[i][5].trim() != 'S')&&(!ele.includes(resultData[i][2]))){
            ele.push(resultData[i][2])
            matrix.push({"av": resultData[i][2],"description": resultData[i][3]})
        }
    }
    //console.log(ele);
    //console.log(matrix);
    //console.log(message);
    message.push({"type": 3}, {"name": sku}, {"BOM": matrix} );
    Response$.emit('message', message);
}



//Funcion de prueba para levantar navegador una vez se desconecta

//Pasa el navegador automatizado de Puppeteer al script automatizador
let exPath="C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe";

//Recibe el configId de opt y se le indica la url de Destino
let pathGo = './resources/puppeteer/.local-chromium/win64-856583/chrome-win/chrome.exe';

//Prueba
async function sesionOFF() {
    if (browser.process() != null) browser.process().kill('SIGINT');
    browser = await puppeteer.launch({
        headless: head, 
        defaultViewport: null, 
        executablePath: exPath,
        ignoreHTTPSErrors: true,
        slowMo: 0});   
}

let savePage;


//browser = await puppeteer.launch({headless: head, defaultViewport: null, executablePath: exPath});

/* browser.on('disconnected', async () => { if (browser.process() != null){
browser.process().kill('SIGINT')
browser = await puppeteer.launch({
headless: head, 
defaultViewport: null, 
executablePath: exPath,
ignoreHTTPSErrors: true,
slowMo: 0});   }
}); */

module.exports = { getSkuSap };

