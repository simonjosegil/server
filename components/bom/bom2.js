const path = require('path');
const { Response$, GetBrowser$ } = require(path.join(__dirname, '../chat/eventemitter'));

let browser;


// 14) Pasa el navegador automatizado de Puppeteer al script automatizador
GetBrowser$.once('msg', (msg) => {
    browser = msg;
}) 

// 15)  Recibe el configId de opt y se le indica la url de Destino
function getBom2(info) {
    let user = info.user.trim();
    let pNumber = info.text.trim();
    matrix = info.content;
    const message2 = new Array();
    let app = info.app.trim();
    let positionJ = info.positionJ
    let url2 = 'https://hpibiplb.corp.hpicloud.net/BOE/OpenDocument/opendoc/openDocument.jsp?sIDType=CUID&iDocID=Fv3WJACu7gsAqRcAAABXmXCiDrmAY7vd';
    vBom2(pNumber, user, matrix, message2, app, positionJ, url2);
}


async function vBom2(pNumber, user, matrix, message2, app, positionJ, url2){
    page3 = await browser.newPage();
    try{
    await page3.goto((url2), {waitUntil: 'load', timeout: 0});

    await page3.waitForSelector('.yui-skin-sam', {waitUntil: 'load', timeout: 400000});
    await page3.click('.yui-skin-sam');     
    await page3.waitForSelector('#openDocChildFrame');

    const elementHandle = await page3.$('#openDocChildFrame');
    const frame = await elementHandle.contentFrame();

    await frame.waitForSelector('#__item79-__xmlview15--promptsList-1', {waitUntil: 'load', timeout: 400000});
    const aHandle = await frame.evaluateHandle(() => document.body);
    const resultHandle = await frame.evaluateHandle(body => body.innerHTML, aHandle);
    await resultHandle.dispose();

    await page3.waitForTimeout(5000);
    await frame.focus('#__item79-__xmlview15--promptsList-0-content');
    await frame.click('#__item79-__xmlview15--promptsList-0-content');
    await page3.waitForTimeout(1000);
    await frame.click('#__xmlview15--lovPanelView--search-I');
    for (x of matrix) {
        await frame.type('#__xmlview15--lovPanelView--search-I', x+";", { delay: 20 });
    };
    await frame.click('#__button87-inner');
    await frame.click('#__button92-inner');
    await page3.waitForTimeout(1000);
    await frame.click('#__button78-inner');
    await frame.waitForSelector('#container_openDocPage_1', {waitUntil: 'load', timeout: 400000});
    await frame.waitForSelector('#viewopenDocPage_1wiseWorkbenchView', {waitUntil: 'load', timeout: 400000});
    await frame.waitForSelector('#__page3', {waitUntil: 'load', timeout: 400000});
    await frame.waitForSelector('#__page3-cont', {waitUntil: 'load', timeout: 400000});
    await frame.waitForSelector('#viewopenDocPage_1wiseWorkbenchView--doubleSidePanelContainer', {waitUntil: 'load', timeout: 400000});
    await frame.waitForSelector('#viewopenDocPage_1wiseWorkbenchView--doubleSidePanelContainer-content-0', {waitUntil: 'load', timeout: 400000});
    await frame.waitForSelector('#viewopenDocPage_1wiseWorkbenchView--doubleSidePanelContainer-content-0', {waitUntil: 'load', timeout: 400000});
    await frame.waitForSelector('#__xmlview6', {waitUntil: 'load', timeout: 400000});
    await frame.waitForSelector('#__item24', {waitUntil: 'load', timeout: 400000});
    await page3.waitForTimeout(15000);
    await frame.focus('#__item24');
    await frame.click('#__item24');
    await frame.waitForSelector('#__item24', {waitUntil: 'load', timeout: 400000});
    await frame.waitForSelector('#__item24', {waitUntil: 'load', timeout: 400000});
    console.log('item24Ok');

    await frame.waitForSelector('table[idref="5.47"] > tbody > tr');
    console.log('Tabla ok');
    tableProcess(page3, pNumber, user, frame); 

}
catch (e) {
    page3.close();
    const msg2 = [{"type": 3}, {"name": 'error'}, {"ALERT": 'Error while loading SKU(SAP)', "ALERTA": 'Error al cargar SKU(SAP)'}];
    Response$.emit('message', msg2); 
    throw new Error(`Servicio no disponible`, 503) 
}


}

async function tableProcess(page3, pNumber, user, frame){
    const selector = 'table[idref="5.47"] > tbody > tr';
    const resultData = await frame.$$eval(selector, rows => {
        return Array.from(rows, row => {
            const columns = row.querySelectorAll('td');
            return Array.from(columns, column => column.innerText.trim());
        });
    });
    createMatrix(resultData, pNumber);
   page3.close();
}


function createMatrix(resultData, pNumber){
    const matrix = new Array();
    const message = new Array();
    const ele = [];

    for(let i=1; i < resultData.length; ++i){
            ele.push(resultData[i][2])
            matrix.push({"av": resultData[i][0],"description": resultData[i][1], "fDescription": resultData[i][12], "plantStatus": resultData[i][27], "eol": resultData[i][30], "availability": resultData[i][31], "discontinued": resultData[i][29], "obsolete": resultData[i][32]})
    }
    //console.log(ele);
    console.log(matrix);
    //console.log(message);
    message.push({"type": 3}, {"name": pNumber}, {"BOM": matrix} );
    Response$.emit('message', message);
}

module.exports = { getBom2 };
