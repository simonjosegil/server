const path = require('path');
const { Response$, GetBrowser$ } = require(path.join(__dirname, '../chat/eventemitter'));
puppeteer = require('puppeteer');


let browser;

// 14) Pasa el navegador automatizado de Puppeteer al script automatizador
GetBrowser$.once('msg', (msg) => {
    browser = msg != null ? msg : browser;
    //browser = msg;
}) 


// 15) Recibe sku y lo procesa junto con la url de destino 
function getSku(info) {
    const preSku = info.text.trim();
    const numeral = info.text.trim().slice(8);
    const preSkuOnly = info.text.trim().slice(0,7)
    const url = 'https://mypim.inc.hpicorp.net/bomlookup/MultiLevelBOMDetail.aspx?PartNumber=';
    const url2 = '%23';
    const url4 = '&partRev=&SiteId=Corporate%5bCORP%5d&PlantName=Corporate%5bCORP%5d&BomUse=[1]&AltBom=1&src=GPG';
    const sku = url.concat(preSkuOnly,url2,numeral,url4);
    skuProcess(sku, preSku);
}

// 16) Script automatizador va hasta la tabla utilizando la URL previamente procesada junto con el sku
async function skuProcess(sku, preSku){
let page2 = await browser.newPage();
try{
        await page2.goto((sku), {waitUntil: 'load', timeout: 50000});
        await page2.waitForSelector('tbody', {waitUntil: 'load', timeout: 50000});
        tableProcess(page2, preSku); 
} catch (e) {
        page2.close();
        const msg2 = [{"type": 3}, {"name": 'error'}, {"ALERT": 'Error while loading SKU(MyPIM)', "ALERTA": 'Error al cargar SKU(MyPIM)'}];
        Response$.emit('message', msg2); 
        throw new Error(`Servicio no disponible`, 503) 
    }
} 

// 17) Procesa todas las columnas de una tabla
async function tableProcess(page2, preSku){
    const selector = 'div#tvBOMLookup > table > tbody > tr > td > font > table > tbody > tr';
    const resultData = await page2.$$eval(selector, rows => {
        return Array.from(rows, row => {
            const columns = row.querySelectorAll('td');
            return Array.from(columns, column => column.innerText.trim());
        });
    });
    createMatrix(resultData, preSku);
   page2.close();
}

// 18) Las columnas se seleccionan a utilizarse de la tabla, se insertan en objeto Json y se responden al websocket
function createMatrix(resultData, preSku){
    const matrix = new Array();
    const message = new Array();
    for(let i=1; i < resultData.length; ++i){
        if(resultData[i][4] != ''){
        matrix.push({"av": resultData[i][4],"description": resultData[i][8]})
        }
    }
    message.push({"type": 3}, {"name": preSku}, {"BOM": matrix} );
    Response$.emit('message', message);
}

//Funcion de prueba para levantar navegador una vez se desconecta

//Pasa el navegador automatizado de Puppeteer al script automatizador
let exPath="C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe";

//Recibe el configId de opt y se le indica la url de Destino
let pathGo = './resources/puppeteer/.local-chromium/win64-856583/chrome-win/chrome.exe';

//Prueba
async function sesionOFF() {
    if (browser.process() != null) browser.process().kill('SIGINT');
    browser = await puppeteer.launch({
        headless: head, 
        defaultViewport: null, 
        executablePath: exPath,
        ignoreHTTPSErrors: true,
        slowMo: 0});   
}

let savePage;


//browser = await puppeteer.launch({headless: head, defaultViewport: null, executablePath: exPath});

/* browser.on('disconnected', async () => { if (browser.process() != null){
browser.process().kill('SIGINT')
browser = await puppeteer.launch({
headless: head, 
defaultViewport: null, 
executablePath: exPath,
ignoreHTTPSErrors: true,
slowMo: 0});   }
}); */

module.exports = { getSku };

