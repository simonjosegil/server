const path = require('path');
const { Response$, GetBrowser$ } = require(path.join(__dirname, '../chat/eventemitter'));

let browser;


// 14) Pasa el navegador automatizado de Puppeteer al script automatizador
GetBrowser$.once('msg', (msg) => {
    browser = msg;
}) 

// 15)  Recibe el configId de opt y se le indica la url de Destino
function getBuild(info) {
    let user = info.user.trim();
    let pNumber = info.text.trim();
    matrix = info.content;
    const message2 = new Array();
    let app = info.app.trim();
    let positionJ = info.positionJ

    validatePCR(pNumber, user, matrix, message2, app, positionJ);
}


async function validatePCR(pNumber, user, matrix, message2, app, positionJ){
    let url2 = 'http://pcrm1.corp.hpicloud.net/PCRMAPI';
    page3 = await browser.newPage();
    await page3.goto((url2), {waitUntil: 'load', timeout: 120000});
    await page3.waitForSelector('.nav-content-wrapper', {waitUntil: 'load', timeout: 120000});
    await page3.evaluate(() => document.getElementById("emailbox").value = "")
    await page3.click('#emailbox');
    await page3.type('#emailbox', user);
    await page3.click('#txtarea1');
    for (x of matrix) {
    await page3.type('#txtarea1', x+"\n");
    };
    await page3.click('#ZSRP_box');
    await page3.click('#SKIP_TAV_box');
    await page3.click('#CHECK_ALL_box');
    //await app === 'occ' ? await page3.click('#BU_FIRST_box') : null;
    await page3.click('#CBU_FIRST_box');
    await page3.click('#CLIC_MODE_box');
    await page3.click('#FILTLOC_box');
    await page3.click('#orderSubmit');
    await page3.waitForSelector('#result > span', {waitUntil: 'load', timeout: 120000});
    const result = await page3.$eval('#result > span', el => el.innerText.trim());
    const txtarea2 = await page3.$eval('#txtarea2', el => el.innerHTML);

    page3.close();

    console.log(result, txtarea2);
    const msg2 = [{"type": 6}, {"name": 'build'}, {"ALERT": result}, {"positionJ": positionJ}, {"txtarea2": txtarea2}];

    Response$.emit('message', msg2);


}







module.exports = { getBuild };
