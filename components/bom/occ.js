const path = require('path');
const { Response$, GetBrowser$ } = require(path.join(__dirname, '../chat/eventemitter'));

let browser;

// 14) Pasa el navegador automatizado de Puppeteer al script automatizador
GetBrowser$.once('msg', (msg) => {
    browser = msg;
}) 

// 15)  Recibe el configId de opt y se le indica la url de Destino
function getConfigId(info) {
    let user = info.user;
    let ConfigId = info.text.trim();
    let url = 'https://occ-ww.austin.hpicorp.net/ngc-maui/InternalLogin?toolMode=SCEPP';
    configIdProcess(ConfigId, url, user);
}


// 16) Script Automatizador va hasta la tabla siguiendo cada uno de los pasos del script 
async function configIdProcess(ConfigId, url, user){
    page2 = await browser.newPage();
    try {
    await page2.goto((url), {waitUntil: 'load', timeout: 120000});
    await page2.waitForSelector('h1', {waitUntil: 'load', timeout: 120000});
    await page2.waitForTimeout(3000);
    await page2.click('#continueButton');
    await page2.waitForTimeout(50);
    await page2.click('#introduction');
    await page2.waitForSelector('#header_search_model', {waitUntil: 'load', timeout: 120000});
    await page2.waitForTimeout(3000);
    await page2.click('#header_search_model');
    await page2.type('#header_search_model', ConfigId);
    await page2.waitForTimeout(1000);
    await page2.click('#header_search_submit');
    await page2.waitForSelector('.searchTbody', {waitUntil: 'load', timeout: 120000});
    await page2.waitForTimeout(3000);
    await page2.click('.searchTbody input');
    await page2.waitForTimeout(1000);
    await page2.click('#detailBtn');
    await page2.waitForSelector('#billDiv', {waitUntil: 'load', timeout: 0});
    await page2.waitForTimeout(3000);
    tableCIProcess(ConfigId, page2, user);
} catch (e) {
    const msg2 = [{"type": 3}, {"name": 'error'}, {"ALERT": 'Error while loading ConfigID', "ALERTA": 'Error al cargar ConfigID'}];
    Response$.emit('message', msg2); 
    page2.close();
    throw new Error(`Servicio no disponible`, 503) 
}
} 

// 17) Procesa todas las columna de una tabla
async function tableCIProcess(ConfigId, page2, user){
    const selector = '.rounded > tbody > tr';
    const selector2 =  '.cfn > span';
     const resultData = await page2.$$eval(selector, rows => {
        return Array.from(rows, row => {
            const columns = row.querySelectorAll('td');
            return Array.from(columns, column => column.innerText.trim());
        });
    }); 

    const getText = await page2.$eval(selector2, el => el.innerText.trim())
    console.log(getText);

    createCIMatrix(resultData, ConfigId, getText, user);
    page2.close();
}
  
// 18) Las columnas se seleecionan a utilizarse de la tabla, se insertan en objeto Json y se responden al websocket
function createCIMatrix(resultData, ConfigId, getText, user){
    const matrix = new Array();
    const message2 = new Array();
    for(let i=1; i < resultData.length; ++i){
        if(resultData[i][3] != undefined){
            matrix.push({"av": resultData[i][3],"description": resultData[i][4]})
        }
    }

    message2.push({"type": 3}, {"name": ConfigId, "name2": getText}, {"BOM": matrix} );
    Response$.emit('message', message2);
   // validatePCR(ConfigId, getText, matrix, message2, user)

} 


async function validatePCR(ConfigId, getText, matrix, message2, user){
    let url2 = 'http://pcrm1.corp.hpicloud.net/PCRMAPI';
    page3 = await browser.newPage();
    await page3.goto((url2), {waitUntil: 'load', timeout: 120000});
    await page3.waitForSelector('.nav-content-wrapper', {waitUntil: 'load', timeout: 120000});
    await page3.evaluate(() => document.getElementById("emailbox").value = "")
    await page3.click('#emailbox');
    await page3.type('#emailbox', user);
    await page3.click('#txtarea1');
    for (x of matrix) {
    await page3.type('#txtarea1', x.av+"\n");
    };
    await page3.click('#ZSRP_box');
    await page3.click('#SKIP_TAV_box');
    await page3.click('#CHECK_ALL_box');
    await page3.click('#BU_FIRST_box');
    await page3.click('#CBU_FIRST_box');
    await page3.click('#CLIC_MODE_box');
    await page3.click('#FILTLOC_box');
    await page3.click('#orderSubmit');
    await page3.waitForSelector('#result > span', {waitUntil: 'load', timeout: 120000});
    const result = await page3.$eval('#result > span', el => el.innerText.trim());
    page3.close();

    console.log(result);
    message2.push({"type": 3}, {"name": ConfigId, "name2": getText, "build": result}, {"BOM": matrix} );
    Response$.emit('message', message2);
}

module.exports = { getConfigId };
