const path = require('path');
const fs = require('fs');
const { domainToASCII } = require('url');
const { Response$, GetBrowser$ } = require(path.join(__dirname, '../chat/eventemitter'));

let browser;


// 14) Pasa el navegador automatizado de Puppeteer al script automatizador
GetBrowser$.once('msg', (msg) => {
    browser = msg;
}) 

// 15)  Recibe el configId de opt y se le indica la url de Destino
function getEcl(info) {
    console.log('Entro a ECL');
    let user = info.user.trim();
    let pNumber = info.text.trim();
    matrix = info.content;
    matrix2= info.content2;
    const message2 = new Array();
    let app = info.app.trim();
    let positionJ = info.positionJ
    validatePCR(pNumber, user, matrix, message2, app, positionJ, matrix2);
    
}


async function validatePCR(pNumber, user, matrix, message2, app, positionJ, matrix2){
    const nameFile = pNumber+'.ecl';
    let matrix3 = new Array();

/*     let url2 = 'http://pcrm1.corp.hpicloud.net/PCRMAPI';
    page3 = await browser.newPage();
    let elemenM;
    await page3.goto((url2), {waitUntil: 'load', timeout: 120000});
    await page3.waitForSelector('.nav-content-wrapper', {waitUntil: 'load', timeout: 120000});
    await page3.evaluate(() => document.getElementById("emailbox").value = "")
    await page3.click('#emailbox');
    await page3.type('#emailbox', user);
    await page3.click('#txtarea1');
    for (x of matrix) {
    await page3.type('#txtarea1', x.trim()+"\n");
    }; */
    matrix.forEach((x, index) => {
        if(x.trim().length === 7){
        const y = matrix2[index];
        matrix3.push(y.trim()+'|'+x.trim()+'|1|'+y.trim()+'|1|');
        }
        if(x.trim().length === 11){
        const w = matrix2[index];
        matrix3.push(w.trim()+'|'+x.trim().slice(0,7)+'|1|'+w.trim()+'|1|');
        matrix3.push(w.trim()+'|'+x.trim()+'|1|'+w.trim()+'|1|');
        matrix3.push(w.trim()+'|'+x.trim()+'|1|'+w.trim()+'|1|');
        }
    });
/*     await page3.click('#ZSRP_box');
    await page3.click('#SKIP_TAV_box');
    await page3.click('#CHECK_ALL_box');
    //await app === 'occ' ? await page3.click('#BU_FIRST_box') : null;
    await page3.click('#CBU_FIRST_box');
    await page3.click('#CLIC_MODE_box');
    await page3.click('#FILTLOC_box');
    await page3.click('#orderSubmit');
    await page3.waitForSelector('#result > span', {waitUntil: 'load', timeout: 120000});
    const result = await page3.$eval('#result > span', el => el.innerText.trim());
    page3.close(); */
    
    console.log(matrix3);

    let matrixTex = "CONFIG_NAME||0||AV"+"|SELECTOR|"+nameFile.trim().slice(0,7)+" #TMP|"+"\n"+ matrix3.join("\n");

    let ECL = 'ECL';
    const file_path =  process.env.NODE_ENV !== "local " ? './resources/ECL/' : './ECL/';
    fs.writeFileSync(file_path + nameFile, matrixTex);

    const msg2 = [{"type": 2}, {"name": "connected"}, {"ALERT": "ECL create in /resources/ECL/", "ALERTA": "ECL creado en /resources/ECL/"}];
    Response$.emit('message', msg2);
}


module.exports = { getEcl };
