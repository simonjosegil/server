const EventEmitter = require('events');

//Constantes que utilizan evenemitter para la comunicación entre archivos y componentes
const Response$ = new EventEmitter();
const GetEntry$ = new EventEmitter();
const GetPing$ = new EventEmitter();
const GetBrowser$ = new EventEmitter();

module.exports = { Response$, GetEntry$, GetPing$, GetBrowser$};