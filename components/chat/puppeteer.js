puppeteer = require('puppeteer');
const path = require('path');
const { Response$, GetPing$, GetBrowser$ } = require(path.join(__dirname, 'eventemitter'));

let page;
let code;
let message2 = new Array();

// 11) Variable boleana de navegador oculto
function getPup(data){
    head = data.text === '' ?  false : data.text[0].active;
    iniPut(head, data);
}

//Navegador solo disponible en modo producción
let pathGo = './resources/puppeteer/.local-chromium/win64-856583/chrome-win/chrome.exe';

// 12) Inicia pupputter y se redirige a la pagina de mypim
async function iniPut(head, data){
    const path =  process.env.NODE_ENV !== "local " ? pathGo : false;
    browser = await puppeteer.launch({
        headless: head, 
        defaultViewport: null, 
        executablePath: path,
        ignoreHTTPSErrors:true,
        timeout:60000,
        args: [ '--disable-web-security', '--disable-features=IsolateOrigins,site-per-process', '--no-sandbox', '--disable-setuid-sandbox', '--proxy-server=https=xxx:xxx' ],
    });
    msg = browser;
    GetBrowser$.emit('msg', msg);
    page = await browser.newPage();
    position = data.text[3].method;
    getFunc(position, page, data);
}    

function getFunc(position, page, data ) {
    const opt  = {
        Default:  () => {assetHub(browser, page)},
        2:  () => {occ(browser, page, data)},
        3: () => {myprs(browser, page, data)},
    }  
    return opt[position] ? opt[position]() : erroComand();
}

async function assetHub(browser, page )
{
    try {
        await page.goto('https://h22189.www2.hp.com', {waitUntil: 'load', timeout: 0});
        await page.waitForSelector('.content', {waitUntil: 'load', timeout: 80000});
        await page.click('#uid_login_link');
        GetPing$.on('ping', (ping) => {proPing(ping)}); 
        await page.waitForSelector('html', {waitUntil: 'load', timeout: 300000});  
        await page.click('html');
        preLoadService(browser, page)
        await page.waitForSelector('.Container', {waitUntil: 'load', timeout: 300000});
        const mail = await page.$eval('.user_and_mb_status', el => el.innerText.trim());
        const msg2 = [{"type": 2}, {"name": "connected"}, {"ALERT": "Welcome "+mail, "ALERTA": "Bienvenido "+mail}, {"USER": mail}];
        Response$.emit('message', msg2); 
        page.close();
    } catch (e) {
        const msg2 = [{"type": 2}, {"name": "errorTimeIni"}, {"ALERT": "Timeout error, shut down and restart the program",  "ALERTA": "Error de tiempo de espera, cierre e inicie nuevamente el programa"}];
        Response$.emit('message', msg2); 
        throw new Error(`Servicio no disponible`, 503) 
    }
}


async function occ(browser, page, data )
{
    try {
        const user= data.text[3].mail;
        const password= data.text[3].password;
        await page.authenticate({username:user, password:password});
        await page.goto('https://occ-ww.austin.hpicorp.net/ngc-maui/InternalLogin?toolMode=SCEPP', {waitUntil: 'load', timeout: 0});   
        GetPing$.on('ping', (ping) => {proPing(ping)}); 
        await page.waitForSelector('html', {waitUntil: 'load', timeout: 300000});  
        await page.click('html');
        preLoadService(browser, page)
        await page.waitForSelector('#dataWrap', {waitUntil: 'load', timeout: 300000});
        const mail = data.text[3].mail;
        const msg2 = [{"type": 2}, {"name": "connected"}, {"ALERT": "Welcome "+mail, "ALERTA": "Bienvenido "+mail}, {"USER": mail}];
        Response$.emit('message', msg2); 
        page.close();
       } catch (e) {
        const msg2 = [{"type": 2}, {"name": "errorTimeIni"}, {"ALERT": "Timeout error, shut down and restart the program",  "ALERTA": "Error de tiempo de espera, cierre e inicie nuevamente el programa"}];
        Response$.emit('message', msg2); 
        throw new Error(`Servicio no disponible`, 503) 
    }  
}


async function myprs(browser, page, data )
{
    try {
        const user= data.text[3].mail;
        const password= data.text[3].password;
        await page.authenticate({username:user, password:password});
        await page.goto('https://myprs.glb.itcs.hpicorp.net/myprs/internal/index.do', {waitUntil: 'load', timeout: 0});   
        GetPing$.on('ping', (ping) => {proPing(ping)}); 
        await page.waitForSelector('html', {waitUntil: 'load', timeout: 300000});  
        await page.click('html');
        //preLoadService(browser, page)
        await page.waitForSelector('.greetingMsg', {waitUntil: 'load', timeout: 300000});
        const mail = data.text[3].mail;
        const msg2 = [{"type": 2}, {"name": "connected"}, {"ALERT": "Welcome "+mail, "ALERTA": "Bienvenido "+mail}, {"USER": mail}];
        Response$.emit('message', msg2); 
        page.close();
       } catch (e) {
        const msg2 = [{"type": 2}, {"name": "errorTimeIni"}, {"ALERT": "Timeout error, shut down and restart the program",  "ALERTA": "Error de tiempo de espera, cierre e inicie nuevamente el programa"}];
        Response$.emit('message', msg2); 
        throw new Error(`Servicio no disponible`, 503) 
    }  
}

async function preLoadService(browser, page){
try {
    await page.waitForSelector('html[dir="ltr"]', {waitUntil: 'load', timeout: 300000}); 
    preMypim(browser);
} catch (e) {
    throw new Error(`Error servicio no disponible`, 503) 
}
} 

async function preMypim(browser){
    let page2 = await browser.newPage();
try {
    await page2.goto('https://mypim.inc.hpicorp.net/default/default.aspx', {waitUntil: 'load', timeout: 60000});
    await page2.waitForSelector('tbody', {waitUntil: 'load', timeout: 60000});
    await page2.close(); 
    console.log("MypingOK");
    preSAP(browser);

} catch (e) { 
    page2.close();
    preSAP(browser);
    throw new Error(`MyPim no disponible`, 503) 
}
}

async function preSAP(){
    let page3 = await browser.newPage();
try {
    await page3.goto('https://hpibiplb.corp.hpicloud.net/BOE/OpenDocument/opendoc/openDocument.jsp?sIDType=CUID&iDocID=Flih8F13_QgAX2sAAADX4KwRAFBWmGHL', {waitUntil: 'load', timeout: 400000});
    await page3.waitForSelector('.yui-skin-sam', {waitUntil: 'load', timeout: 400000});
    await page3.click('.yui-skin-sam');
    await page3.waitForSelector('.yui-layout-wrap', {waitUntil: 'load', timeout: 400000});
    await page3.click('.yui-layout-wrap');
    await page3.waitForSelector('#openDocChildFrame', {waitUntil: 'load', timeout: 400000});
    const elementHandle = await page3.waitForSelector('#openDocChildFrame', {waitUntil: 'load', timeout: 400000});
    const frame = await elementHandle.contentFrame();
    await frame.waitForSelector('.cssWebIThemeClass', {waitUntil: 'load', timeout: 400000});
    await frame.waitForSelector('.sapMSFI', {waitUntil: 'load', timeout: 400000});
    await page3.close();
    console.log('PreSapOk');

} catch (e) { 
    page3.close();
    throw new Error(`Sap no disponible`, 503) 
}
}


// 13) Ingresa Pin desde ventana principal
async function proPing(ping){ 
    await page.click('#otp');
    code = await ping.toString(); 
    //console.log(code); 
    await page.type('#otp', code, {waitUntil: 'networkidle0', timeout: 0});
    await page.waitForTimeout(1000);
    await page.click('.buttons input');
    await page.waitForSelector('.dialog .window .error-message', {waitUntil: 'load', timeout: 0});
    errorCode();
}

// 13.1) Saca error si el cordigo es incorrecto
async function errorCode(){
    const content = await page.$eval('.dialog .window .error-message', el => el.innerText);
    //console.log(content);
    if('Invalid passcode' || 'Clave de acceso no v├ílida' === content.trim()){
    const msg3 = [{"type": 2}, {"name": "errorPing"}, {"ALERT": "Invalid Ping", "ALERTA": "Ping inválido"}];
    Response$.emit('message', msg3); 
    }
}


module.exports = { getPup };



//browser = await puppeteer.launch({headless: head, defaultViewport: null, executablePath: exPath});

/* browser.on('disconnected', async () => { if (browser.process() != null){
browser.process().kill('SIGINT')
browser = await puppeteer.launch({
headless: head, 
defaultViewport: null, 
executablePath: exPath,
ignoreHTTPSErrors: true,
slowMo: 0});   }
}); */