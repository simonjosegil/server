const path = require('path');
const { entry } = require(path.join(__dirname, 'opt'));
const { Response$, GetEntry$ } = require(path.join(__dirname, 'eventemitter'));
const { io } = require(path.join(__dirname, '../../server'));

// 6) Socket emite y recibe respuestas del cliente
io.on('connection', function(socket){
    // 7) Recibe todas las peticiones chat emite una respuesta cliente(Nube de mensajes)
    socket.on('send-message', function(data, callback){
        const myMessages = [];
        myMessages.push(data);
        entry(data);
        //console.log(myMessages);
        socket.emit('text-event', myMessages)

        // 19) Recibe las respuestas de los scripts automatizados y las envía al cliente
        Response$.once('message', function(message) {
            const dt = message;
            callback({ resp: dt})
            console.log(dt);
        })
})
})