
const path = require('path');
const { getSku } = require(path.join(__dirname, '../bom/mypim'));
const { getSkuSap } = require(path.join(__dirname, '../bom/sap'));
const { getConfigId } = require(path.join(__dirname, '../bom/occ'));
const { getBuild } = require(path.join(__dirname, '../bom/build'));
const { getBom2 } = require(path.join(__dirname, '../bom/bom2'));
const { getEcl } = require(path.join(__dirname, '../bom/ecl'));
const { getPup } = require(path.join(__dirname, './puppeteer'));
const { getPrice } = require(path.join(__dirname, '../bom/price'));
const { GetPing$, Response$ } = require(path.join(__dirname, 'eventemitter'));

msg = [{"type": 1}, {"name": 'error'}, {"BOM": 'error'}];

// 8) Valida si la información es un comando o la variable de interfaz
 async function entry(data){
    console.log(data);
    const inf = data;
    const tipo = inf.type;
    tipo === 0 ? getPup(inf) : go(inf);
    console.log(tipo);
}  

// 9) Asigna la posición del comando
async function go(data){
    const info = await data;
    const position = await info.positionN;
    await getFunc(position, info);
}

// 10) Ejecuta el comando de acuerdo a su posición
function getFunc(position, info) {
    const opt  = {
        6:  () => {const ping = info.text.trim();  GetPing$.emit('ping', ping)},
        8:  () => {getConfigId(info)},
        11: () => {info.app === 'mypim' ? getSku(info): getSkuSap(info)},
        15: () => {getBuild(info)},
        16: () => {getEcl(info)},
        17: () => {getBom2(info)},
        19: () => {getPrice(info)}
    }  
    return opt[position] ? opt[position]() : erroComand();
}

function erroComand(){
    const message = [{"type": 1}, {"name": 'error'}, {"ALERT": 'Error, Unexisting command', "ALERTA": 'Error, comando inexistente'}];
    Response$.emit('message', message);
}

module.exports = { entry };


