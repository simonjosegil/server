const { app, BrowserWindow } = require('electron');
const url = require('url');
const path = require('path');



let mainWindow;

// 6.1) Levanta la interfaz de Electron para aplicación web como APP de escritorio
app.on('ready', ()=> {
    mainWindow = new BrowserWindow({
        title: 'Presales',
        icon: path.join(__dirname, '../../assets/icons/win/presales4.ico')
    });
    mainWindow.loadURL(url.format({
        pathname: 'localhost:3000',
        protocol: 'http:',
        slashes: true,
        hash: '/contact'
    }))
});



/* 
const gotTheLock = app.requestSingleInstanceLock();


if (gotTheLock) {
    //logger.info('Hay una segunda instancia');
    app.quit();
} else {
app.on('ready', ()=> {
    mainWindow = new BrowserWindow({
        title: 'Presales Online',
        icon: path.join(__dirname, '../../assets/icons/win/presales4.ico')
    });
    mainWindow.loadURL(url.format({
        pathname: 'localhost:3000',
        protocol: 'http:',
        slashes: true,
        hash: '/contact'
    }))
});
} */